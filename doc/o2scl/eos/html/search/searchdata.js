var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "abcdefghijlmnopqrstuv",
  2: "bfo",
  3: "abcdefghijlmnopqrstuv",
  4: "abcdefghijklmnopqrstuvwxz~",
  5: "_abcdefghijklmnopqrstuvwxyz",
  6: "abdefghijmnopt",
  7: "o",
  8: "o",
  9: "abcdefghilmnoprstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "related",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Friends",
  9: "Pages"
};

