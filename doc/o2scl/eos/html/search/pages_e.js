var searchData=
[
  ['roots_20of_20polynomials',['Roots of Polynomials',['../../../html/poly_section.html',1,'(Global Namespace)'],['../../../part/html/poly_section.html',1,'(Global Namespace)']]],
  ['related_20projects',['Related Projects',['../../../html/related_section.html',1,'(Global Namespace)'],['../../../part/html/related_section.html',1,'(Global Namespace)']]],
  ['random_20number_20generation',['Random Number Generation',['../../../html/rng_section.html',1,'(Global Namespace)'],['../../../part/html/rng_section.html',1,'(Global Namespace)']]],
  ['rotating_20neutron_20stars',['Rotating Neutron Stars',['../rotpage.html',1,'index']]]
];
