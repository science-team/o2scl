var searchData=
[
  ['least_2dsquares_20fitting',['Least-Squares Fitting',['../../../html/fit_section.html',1,'(Global Namespace)'],['../../../part/html/fit_section.html',1,'(Global Namespace)']]],
  ['license_20information',['License Information',['../../../html/license_section.html',1,'(Global Namespace)'],['../../../part/html/license_section.html',1,'(Global Namespace)']]],
  ['linear_20algebra',['Linear Algebra',['../../../html/linalg_section.html',1,'(Global Namespace)'],['../../../part/html/linalg_section.html',1,'(Global Namespace)']]],
  ['library_20settings',['Library Settings',['../../../html/lset_section.html',1,'(Global Namespace)'],['../../../part/html/lset_section.html',1,'(Global Namespace)']]]
];
