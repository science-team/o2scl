var searchData=
[
  ['design_20considerations',['Design Considerations',['../../../html/design_section.html',1,'(Global Namespace)'],['../../../part/html/design_section.html',1,'(Global Namespace)']]],
  ['developer_20guidelines',['Developer Guidelines',['../../../html/dev_page.html',1,'(Global Namespace)'],['../../../part/html/dev_page.html',1,'(Global Namespace)']]],
  ['differentiation',['Differentiation',['../../../html/diff_section.html',1,'(Global Namespace)'],['../../../part/html/diff_section.html',1,'(Global Namespace)']]],
  ['download_20o2scl',['Download O2scl',['../../../html/dl_page.html',1,'(Global Namespace)'],['../../../part/html/dl_page.html',1,'(Global Namespace)']]],
  ['data_20tables',['Data Tables',['../../../html/table_section.html',1,'(Global Namespace)'],['../../../part/html/table_section.html',1,'(Global Namespace)']]],
  ['development_20team',['Development Team',['../../../html/team_section.html',1,'(Global Namespace)'],['../../../part/html/team_section.html',1,'(Global Namespace)']]]
];
